---
title: "Home"
date: 2018-02-10T18:56:13-05:00
sitemap:
  priority : 1.0

outputs:
- html
- rss
- json
---
<p>Soy una persona con experiencia en atención al cliente, capacidad de trabajo en equipo, organizada, responsable y comprometida con los objetivos fijados.
Me gusta adquirir conocimientos que fomenten mi creatividad y desarrollo profesional.</p>
