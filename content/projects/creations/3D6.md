{
  "title": "Cocina 4",
  "date": "2018-01-15T12:41:05-05:00",
  "image": "/img/3D6.png",
  "description": "Infografía 3D para Juan Alves S.L: Sketchup + V-ray. Tamaño: 1920 x 1080.",
  "tags": ["Diseño","cocina"],
  "fact": "",
  "featured":true
}
Infografía 3D para Juan Alves S.L.

Modelado y renderizado: Sketchup + V-ray.
Tamaño: 1920 x 1080